package jcryptoken;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class Token {
	private static final String DEFAULT_ENCODING = "UTF-8";

	private Map<String, String> attributes = new HashMap<>();

	private String id;

	private Date expiresAt;

	private byte[] key;

	public Token(byte[] key) {
		this.key = key;
	}

	public Token(byte[] key, String encryptedToken) {
		this(key);
		load(encryptedToken);
	}

	public Token setId(String id) {
		this.id = id;
		return this;
	}

	public Token set(String name, String value) {
		attributes.put(name, value);
		return this;
	}

	public Token del(String name) {
		attributes.remove(name);
		return this;
	}

	public String get(String name) {
		return attributes.get(name);
	}

	public String getId() {
		return id;
	}

	public Date getExpiresAt() {
		return expiresAt;
	}

	public Token setExpiresAt(Date expiresAt) {
		this.expiresAt = expiresAt;
		return this;
	}

	public boolean isExpired() {
		return null == expiresAt ? true : expiresAt.getTime() < System
				.currentTimeMillis();
	}
	
	public boolean isValid() {
		return null != getId() && !getId().isEmpty() && !isExpired();
	}

	private void load(String encryptedToken) {
		String[] fields = decrypt(encryptedToken).split("\\|");
		try {
			setId(fields[0]);
			setExpiresAt(new Date(Long.valueOf(fields[1])));
			for (int i = 2; i < fields.length; i++) {
				String[] pair = fields[i].split("=");
				set(pair[0], pair[1]);
			}
		} catch (Throwable t) {
			throw new InvalidTokenException("failed parsing token", t);
		}
	}

	private String decrypt(String token) {
		try {
			Cipher cipher = Cipher.getInstance("AES");
			SecretKey secKey = new SecretKeySpec(key, "AES");
			cipher.init(Cipher.DECRYPT_MODE, secKey);
			return new String(cipher.doFinal(Base64.decodeBase64(token
					.getBytes())), DEFAULT_ENCODING);
		} catch (Exception e) {
			throw new TokenDecryptionException(e);
		}
	}

	@Override
	public String toString() {
		if (null == id)
			throw new InvalidTokenAttributeException("id should not be null");
		if (null == expiresAt)
			throw new InvalidTokenAttributeException(
					"expiration date should not be null");

		String token = String.format("%s|%d", id, expiresAt.getTime());
		for (Map.Entry<String, String> e : attributes.entrySet()) {
			if (null == e.getKey())
				throw new InvalidTokenAttributeException(
						"attribute name should not be null");
			if (e.getKey().isEmpty())
				throw new InvalidTokenAttributeException(
						"attribute name should not be empty");
			if (null == e.getValue())
				throw new InvalidTokenAttributeException(
						"attribute value should not be null");
			token += String.format("|%s=%s", e.getKey(), e.getValue());
		}
		return encrypt(token);
	}

	private String encrypt(String token) {
		try {
			SecretKey secKey = new SecretKeySpec(key, "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, secKey);
			return Base64.encodeBase64String(cipher.doFinal(token
					.getBytes(DEFAULT_ENCODING)));
		} catch (Exception e) {
			throw new TokenEncryptionException(e);
		}
	}
	
	public Token expiresIn(int unit, Duration period) {
		setExpiresAt(period.fromNow(unit));
		return this;
	}

}
