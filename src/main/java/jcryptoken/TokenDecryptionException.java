package jcryptoken;

public class TokenDecryptionException extends RuntimeException {

	public TokenDecryptionException(Throwable t) {
		super(t);
	}

	private static final long serialVersionUID = 8636605392393286976L;

}
