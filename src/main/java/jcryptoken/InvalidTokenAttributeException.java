package jcryptoken;

public class InvalidTokenAttributeException extends RuntimeException {

	public InvalidTokenAttributeException(String msg) {
		super(msg);
	}

	private static final long serialVersionUID = 8636605592399286976L;

}
