package jcryptoken;

public class InvalidTokenException extends RuntimeException {

	public InvalidTokenException(String msg) {
		super(msg);
	}

	public InvalidTokenException(String msg, Throwable cause) {
		super(msg, cause);
	}

	private static final long serialVersionUID = 8631605592399286976L;

}
