package jcryptoken;

public class TokenEncryptionException extends RuntimeException {

	public TokenEncryptionException(Throwable t) {
		super(t);
	}

	private static final long serialVersionUID = 8636605592393286976L;

}
